var path = require("path");
var express = require("express");
var app = express();

// Where do we find the static files for the simple site?

app.use(express.static("static/meta"));
app.use("/static", express.static("static"));

// Define routes.

app.get("/", function (req, res) {
    let hostname = req.get("host");
    let domainParts = hostname.split(".");

    if (hostname == "half-baked-redirector.appspot.com" || domainParts.length < 3) {
        // Handle normal website.

        res.sendFile(path.join(__dirname + "/index.html"));
    } else if (domainParts.length == 3) {
        // Perform a redirect.

        let subdomain = domainParts[0];

        // This is such a simple app, we might as well just hardcode.

        switch (subdomain) {
            case "inbox":
                res.redirect(301, "https://inbox.google.com/");
                break;
            case "issues":
            case "jira":
                res.redirect(301, "https://halfbakedstudio.atlassian.net/");
                break;
            case "docs":
            case "confluence":
            case "wiki":
                res.redirect(301, "https://halfbakedstudio.atlassian.net/wiki/");
                break;
            case "prismic":
            case "cms":
                res.redirect(301, "https://halfbakedstudio.prismic.io/");
                break;
            case "build":
                res.redirect(301, "https://console.cloud.google.com/cloud-build/");
                break;
            case "gcp":
            case "cloud":
                res.redirect(301, "https://console.cloud.google.com/");
                break;
            case "cloudflare":
            case "proxy":
                res.redirect(301, "https://cloudflare.com/");
                break;
            case "bitbucket":
            case "vcs":
            case "mercurial":
            case "svc":
            case "source":
            case "git":
                res.redirect(301, "https://bitbucket.org/halfbakedstudio/");
                break;
            case "chat":
            case "discord":
                res.redirect(301, "https://discord.gg/pke8ZFJ");
                break;
            case "storage":
            case "artifacts":
                res.redirect(301, "https://console.cloud.google.com/storage/");
                break;
            case "codacy":
            case "qualimetry":
                res.redirect(301, "https://app.codacy.com/app/halfbakedstudio/");
                break;
            case "id":
            case "people":
                res.redirect(301, "https://halfbakedstudio.atlassian.net/people/");
                break;
            case "namecheap":
            case "dns":
                res.redirect(301, "https://namecheap.com/");
                break;
            case "engine":
            case "unity":
                res.redirect(301, "https://docs.unity3d.com/Manual/index.html");
                break;
            case "gsuite":
            case "suite":
            case "admin":
                res.redirect(301, "https://admin.google.com/");
                break;
            default:
                res.redirect(301, "https://halfbaked.tools/");
                break;
        }
    } else {
        res.redirect(301, "https://halfbaked.tools/");
    }
});

// Start listening.

app.listen(8080, function () {
    console.log("Running on port 8080.");
});
