# redirector

Simple Node application to redirect requests to other locations. Useful for an organisation's toolset.

## Note

The code quality here is really terrible. Hardcoded everywhere, not really maintainable. Fixing the code, however, is not a priority as it is only for internal use.

This repository is currently shifting to `git flow`, a proper CI/CD framework based on tagging, and to use the `Nuxt` JavaScript framework (on top of Vue.js). It is not yet ready for a deployment to GCP.

## Build

As usual with npm projects:

```sh
npm install
npm start
```

This will run a redirector on 0:8080. Any request it sees will first check for subdomains. Then it will either serve or redirect to serve the main page with the cheatsheet.
